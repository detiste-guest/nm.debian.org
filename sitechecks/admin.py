from django.contrib import admin
from .models import Inconsistency


class InconsistencyAdmin(admin.ModelAdmin):
    ...


admin.site.register(Inconsistency, InconsistencyAdmin)
