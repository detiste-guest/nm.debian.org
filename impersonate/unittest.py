from __future__ import annotations
from django.utils.module_loading import import_string
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings
from signon import providers

_name = getattr(settings, "IMPERSONATE_TEST_FIXTURE", None)
if _name is None:
    raise ImproperlyConfigured(
        "You need to set IMPERSONATE_TEST_FIXTURE to a valid dotted module.class path, to use impersonate unittests")


class ImpersonateFixtureMixin(import_string(_name)):
    def setUp(self):
        super().setUp()
        # Reset the providers cache before each test
        providers.providers_by_name = None


__all__ = ["ImpersonateFixtureMixin"]
