from django.test import TestCase, override_settings
from django.urls import reverse
from django.core import mail
from backend.models import Person
from backend import const
from backend.unittest import PersonFixtureMixin
from signon import providers


@override_settings(SIGNON_PROVIDERS=[
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
    providers.BaseSessionProvider(name="salsa", label="Salsa", single_bind=True),
])
class TestNewnm(PersonFixtureMixin, TestCase):
    # Use an old, not yet revoked key of mine
    new_person_fingerprint = "66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB"

    @classmethod
    def __add_extra_tests__(cls):
        for person in ("pending", "dc", "dc_ga", "dm", "dm_ga"):
            cls._add_method(cls._test_non_dd, person, "debsso")
            cls._add_method(cls._test_non_dd, person, "salsa")

        for person in ("dd_nu", "dd_u", "fd", "dam"):
            cls._add_method(cls._test_dd, person, "debsso")
            cls._add_method(cls._test_dd, person, "salsa")

        cls._add_method(cls._test_no_person, "debsso")
        cls._add_method(cls._test_no_person, "salsa")

    def get_identity_for_issuer(self, person, issuer):
        if issuer == "debsso":
            person = self.persons[person]
            if person is None:
                uid = "new_person"
            else:
                uid = person.ldap_fields.uid
            return self.identities.create(
                    "debsso", issuer="debsso",
                    subject=f"{uid}@users.alioth.debian.org",
                    username=f"{uid}@users.alioth.debian.org", audit_skip=True)
        elif issuer == "salsa":
            person = self.persons[person]
            if person is None:
                subject = "1234567"
            else:
                subject = str(person.pk)

            return self.identities.create(
                    "salsa", issuer="salsa",
                    subject=subject, audit_skip=True)
        else:
            raise NotImplementedError("Issuer {issuer} not supported in test")

    def assertPostForbidden(self, person):
        person = self.persons[person]
        client = self.make_test_client(person)

        # Posting to newnm to create a new record is forbidden
        response = client.post(reverse("public_newnm"), data={
            "person-fpr": self.new_person_fingerprint,
            "person-sc_ok": "yes",
            "person-dmup_ok": "yes",
            "person-fullname": "test",
            "ldap_fields-cn": "test",
            "person-email": "new_person@example.org"})
        self.assertPermissionDenied(response)

        # Trying to resend newnm challenge is forbidden
        if person:
            response = client.get(reverse(
                "public_newnm_resend_challenge", kwargs={"key": person.lookup_key}))
        else:
            response = client.get(reverse(
                "public_newnm_resend_challenge", kwargs={"key": self.persons["pending"].lookup_key}))
        self.assertPermissionDenied(response)

        # Trying to confirm the account is forbidden
        if person:
            response = client.get(reverse("public_newnm_confirm", kwargs={"nonce": person.pending}))
        else:
            response = client.get(reverse("public_newnm_confirm", kwargs={"nonce": self.persons["pending"].pending}))
        self.assertPermissionDenied(response)

    def test_require_login(self):
        client = self.make_test_client(None)
        response = client.get(reverse("public_newnm"))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["person"].is_authenticated)
        self.assertEqual(response.context["errors"], [])
        self.assertEqual(response.context["DAYS_VALID"], 3)
        self.assertContains(response, "Please login first")
        self.assertNotContains(response, "You already have an entry in the system")
        self.assertNotContains(response, "Not only do you already have an entry, but you are also")
        self.assertNotContains(response, "Apply for an entry in the system")
        self.assertNotContains(response, "Submit disabled because you already have an entry in the system")
        self.assertPostForbidden(None)

    def _test_no_person(self, issuer):
        self.maxDiff = None
        identity = self.get_identity_for_issuer(None, issuer)
        client = self.make_test_client(None, [identity])
        response = client.get(reverse("public_newnm"))
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context["require_login"])
        self.assertFalse(response.context["person"].is_authenticated)
        self.assertEqual(response.context["errors"], [])
        self.assertEqual(response.context["DAYS_VALID"], 3)
        self.assertNotContains(response, "Please login first")
        self.assertNotContains(response, "You already have an entry in the system")
        self.assertNotContains(response, "Not only do you already have an entry, but you are also")
        self.assertContains(response, "Apply for an entry in the system")
        self.assertNotContains(response, "Submit disabled because you already have an entry in the system")

        # A new Person is created on POST
        response = client.post(reverse("public_newnm"), data={
            "person-fpr": self.new_person_fingerprint,
            "person-sc_ok": "yes",
            "person-dmup_ok": "yes",
            "person-fullname": "test",
            "ldap_fields-cn": "test",
            "person-email": "new_person@example.org"})
        self.assertRedirectMatches(
                response, reverse("public_newnm_resend_challenge", kwargs={"key": "new_person@example.org"}))
        new_person = Person.lookup("new_person@example.org")
        self.assertEqual(new_person.status, const.STATUS_DC)
        self.assertIsNotNone(new_person.expires)
        self.assertIsNotNone(new_person.pending)

        # The new person can resend the challenge email
        mail.outbox = []
        response = client.get(reverse("public_newnm_resend_challenge", kwargs={"key": "new_person@example.org"}))
        self.assertRedirectMatches(response, new_person.get_absolute_url())
        self.assertEqual(len(mail.outbox), 1)
        m = mail.outbox[0]
        self.assertEqual(m.from_email, "nm@debian.org")
        self.assertEqual(m.to, [f"test <new_person@example.org>"])
        self.assertEqual(m.cc, [])
        self.assertEqual(m.bcc, [])
        self.assertEqual(m.extra_headers, {'Reply-To': 'nm@debian.org'})
        head = """Hello test,

to confirm your new entry at https://nm.debian.org/person/new_person@example.org/
you need to decrypt the following text. The result will be a URL, which you
then visit to make your entry confirmed.

-----BEGIN PGP MESSAGE-----
""".splitlines()
        tail = """-----END PGP MESSAGE-----


You should not need instructions to decrypt this. If you do not know how to do
it, you can consider it a challenge. In that case, you can start from here:
https://www.gnupg.org/howtos/en/GPGMiniHowto.html

For any problem, feel free to contact Front Desk at nm@debian.org.


Regards,

the nm.debian.org robotic minion for Front Desk""".splitlines()
        lines = m.body.splitlines()
        self.assertEqual(lines[:len(head)], head)
        self.assertEqual(lines[-len(tail):], tail)

        # The new person has a page in the system
        response = client.get(new_person.get_absolute_url())
        self.assertEqual(response.status_code, 200)

        # The new person can confirm its record
        response = client.get(reverse("public_newnm_confirm", kwargs={"nonce": new_person.pending}))
        self.assertRedirectMatches(response, new_person.get_absolute_url())
        new_person = Person.objects.get(pk=new_person.pk)
        self.assertEqual(new_person.status, const.STATUS_DC)
        self.assertIsNotNone(new_person.expires)
        self.assertEqual(new_person.pending, "")

    def _test_non_dd(self, person, issuer):
        identity = self.get_identity_for_issuer(person, issuer)
        client = self.make_test_client(person, [identity])
        response = client.get(reverse("public_newnm"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["person"], self.persons[person])
        self.assertEqual(response.context["errors"], [])
        self.assertEqual(response.context["DAYS_VALID"], 3)
        self.assertNotContains(response, "Please login first")
        self.assertContains(response, "You already have an entry in the system")
        self.assertNotContains(response, "Not only do you already have an entry, but you are also")
        self.assertNotContains(response, "Apply for an entry in the system")
        self.assertNotContains(response, "Submit disabled because you already have an entry in the system")
        self.assertPostForbidden(None)

    def _test_dd(self, person, issuer):
        identity = self.get_identity_for_issuer(person, issuer)
        client = self.make_test_client(person, [identity])
        response = client.get(reverse("public_newnm"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["person"], self.persons[person])
        self.assertEqual(response.context["errors"], [])
        self.assertEqual(response.context["DAYS_VALID"], 3)
        self.assertNotContains(response, "Please login first")
        self.assertContains(response, "You already have an entry in the system")
        self.assertContains(response, "Not only do you already have an entry, but you are also")
        self.assertContains(response, "Apply for an entry in the system")
        self.assertContains(response, "Submit disabled because you already have an entry in the system")
        self.assertPostForbidden(None)
