# coding: utf-8
"""
Test person view
"""
from django.test import TestCase
from backend import const
from backend.unittest import PersonFixtureMixin


class PersonPageTestCase(PersonFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Create a test person and process
        cls.persons.create("test", status=const.STATUS_DC, cn="Test", sn="Test", email="testuser@debian.org")
        cls.processes.create("app", person=cls.persons.test, applying_for=const.STATUS_DM)

    def test_person_listing(self):
        """
        Tests that a person is listed on the public persons page
        """
        # Check that the new person is listed on the page
        client = self.make_test_client(None)
        response = client.get('/public/people/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Test Test', 1)
        self.assertContains(response, '>test<', 1)
