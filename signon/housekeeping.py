from __future__ import annotations
import os
import json
from django.conf import settings
from django.contrib.auth import get_user_model
import logging
import django_housekeeping as hk
from signon.models import Identity
import gitlab

log = logging.getLogger(__name__)

STAGES = ["backup", "main"]


class BackupIdentities(hk.Task):
    """
    Backup user/identifier associations
    """
    def run_backup(self, stage):
        if not self.hk.outdir:
            return
        outfile = os.path.join(self.hk.outdir.path("backup"), "signon_identities.json")

        res = []
        for ident in Identity.objects.all():
            res.append({
                "issuer": ident.issuer,
                "subject": ident.subject,
                "profile": ident.profile,
                "picture": ident.picture,
                "fullname": ident.fullname,
                "username": ident.username,
                "person": str(ident.person) if ident.person else None,
            })

        with open(outfile, "wt") as out:
            json.dump(res, out, indent=1)


# FIXME - figure wether we really want this, and if we do get a SALSA_TOKEN
# class SalsaSync(hk.Task):
class SalsaSync():
    """
    Sync account information with Salsa
    """
    def run_main(self, stage):
        dry_run = self.hk.dry_run

        housekeeper = get_user_model().objects.get_housekeeper()

        salsa_host = getattr(settings, "SALSA_HOST", "https://salsa.debian.org")
        token = getattr(settings, "SALSA_TOKEN", None)
        if token is None:
            log.warn("%s: SALSA_TOKEN must be defined in settings", self.IDENTIFIER)

        # Load Salsa username -> id mapping
        by_subject = {}
        gl = gitlab.Gitlab(salsa_host, private_token=token)
        for user in gl.users.list(all=True):
            by_subject[str(user.id)] = {
                "profile": user.web_url,
                "picture": user.avatar_url,
                "fullname": user.name,
                "username": user.username,
            }
        log.info("%s: %d users found by salsa API", self.IDENTIFIER, len(by_subject))

        # Update Salsa identities
        salsa_identities = 0
        not_matched = 0
        for identity in Identity.objects.filter(issuer="salsa"):
            salsa_identities += 1
            info = by_subject.get(identity.subject)
            if info is None:
                not_matched += 1
                continue
            if dry_run:
                continue
            changed = identity.update(
                profile=info.get("profile"),
                picture=info.get("picture"),
                fullname=info.get("name"),
                username=info.get("nickname"),
                audit_author=housekeeper,
                audit_notes="updated from salsa API")
            if changed:
                log.info("%s: %s salsa information updated",
                         self.IDENTIFIER, identity)

        log.info("%s: %d salsa identities known, %d not found in salsa", self.IDENTIFIER, salsa_identities, not_matched)
