from backend.mixins import VisitPersonMixin
from django.views.generic import TemplateView
from .models import Process


class VisitProcessMixin(VisitPersonMixin):
    """
    Visit a person process. Adds self.person, self.process and self.visit_perms with
    the permissions the visitor has over the person
    """
    def get_person(self):
        return self.process.person

    def get_process(self):
        return Process.lookup_or_404(self.kwargs["key"])

    def get_visit_perms(self):
        return self.process.permissions_of(self.request.user)

    def load_objects(self):
        self.process = self.get_process()
        super(VisitProcessMixin, self).load_objects()

    def get_context_data(self, **kw):
        ctx = super(VisitProcessMixin, self).get_context_data(**kw)
        ctx["process"] = self.process
        return ctx


class VisitProcessTemplateView(VisitProcessMixin, TemplateView):
    template_name = "process/show.html"
