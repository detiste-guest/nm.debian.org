from rest_framework import serializers
import signon.models as snmodels
from backend.serializer_fields import PersonKeyField

class IdentityExportSerializer(serializers.ModelSerializer):
    person = PersonKeyField(allow_null=True)

    class Meta:
        model = snmodels.Identity
        exclude = ["id"]

class IdentityRedactedExportSerializer(serializers.ModelSerializer):
    person = PersonKeyField(allow_null=True)

    class Meta:
        model = snmodels.Identity
        fields = (
            'person', 'issuer', 'subject', 'profile',
            'picture','fullname', 'username')
