Debian NM Front Desk web application
====================================

WARNING: project moved to https://salsa.debian.org/nm-team/nm.debian.org

## Running this code on your own machine
### Dependencies

See `.gitlab-ci.yml` for an up to date list of dependencies.

### Configuration

    mkdir data # required by default settings
    cd nm2
    ln -s local_settings.py.devel local_settings.py
    edit local_settings.py as needed

### First setup
    
    ./manage.py migrate

### Fill in data
Visit https://nm.debian.org/am/db-export to download nm-mock.json; for privacy,
sensitive information are replaced with mock strings.

If you cannot login to the site, you can ask any DD to download it for you.
There is nothing secret in the file, but I am afraid of giving out convenient
email databases to anyone.

    ./manage.py import nm-mock.json

If you are a Front Desk member or a DAM and want a full database export, you
need to use

    ./manage.py export --full

### Reset database
    rm data/db-used-for-development.sqlite
    ./manage.py migrate
    ./manage.py import nm-mock.json

### Run database maintenance
    
    ./manage.py housekeeping

### Run the web server
    
    ./manage.py runserver


## Periodic updates
You need to run periodic maintenance to check/regenerate the de-normalised
fields:

    ./manage.py housekeeping


## Development
Development targets Django 1.8, although the codebase has been created with
Django 1.2 and it still shows in some places. Feel free to cleanup.

Unusual things in the design:

* `backend/` has the core models. Note that `backend.models.Process` is the
  old-style workflow, and `process.model.Process` is the new style workflow.
* there is a custom permission system with a class hierarchy that starts at
  `backend.models.VisitorPermissions`, and that generates a set of permission
  strings that get tested in views and templates with things like `if
  "edit_bio" in visit_perms: …`.
* `backend.mixins.VisitorMixin` is the root of a class hierarchy of mixins used
  by most views in the site; those mixins implement the basis of resource
  instantiation and permission checking.

## WebUI Translation
The NM Front Desk web application is supporting localization's due the
possibility of build in l10n support. Currently there are the languages German,
French, Spanish and Italian prepared. You might want to help out to improve the
existing l10n strings or add more languages. To do this you will need the
following steps.

Updating the current strings of the language you want to work on. E.g. for
German. (Note: If you don't use a single language as specification all
languages will be updated.)

    ./manage.py makemessages -l de

Next simply use the preferred tool for working on the .po file which can be
found in the folder `locale/$(LANG)/LC_MESSAGES/django.po`.

To test your work you need to create the compiled catalog of the l10n strings.

    ./manage.py compilemessages

Next run the local Django webserver.

    ./manage.py runserver

Control your work with the preferred browser by calling the URI
'localhost:8000'. Note, you need to call `./manage.py compilemessages` again
once you have modified the *.po file in order to improve the preferred
translation.
