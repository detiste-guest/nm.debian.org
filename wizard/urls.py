from __future__ import annotations
from django.urls import path
from backend.mixins import VisitorTemplateView
from . import views

app_name = "wizard"

urlpatterns = [
    path("", VisitorTemplateView.as_view(template_name="wizard/home.html"), name="home"),
    path("advocate/", views.Advocate.as_view(), name="advocate"),
    path("process/<applying_for>/", views.NewProcess.as_view(), name="newprocess"),
]
