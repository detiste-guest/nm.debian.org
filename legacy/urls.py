from __future__ import annotations
from django.urls import path
from . import views

app_name = "legacy"

urlpatterns = [
    path('process/<key>/', views.Process.as_view(), name="process"),
    path('mail-archive/<key>/', views.MailArchive.as_view(), name="download_mail_archive"),
    path('display-mail-archive/<key>/', views.DisplayMailArchive.as_view(), name="display_mail_archive"),
]
