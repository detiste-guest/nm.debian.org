from django.test import TestCase
from django.urls import reverse
from backend.unittest import BaseFixtureMixin, NamedObjects
from backend import const
from apikeys.models import Key


class TestStatusAllPermissions(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(keys=NamedObjects(Key))

        p = cls.create_person("dc", status=const.STATUS_DC, alioth=True)
        cls.keys.create("dc", user=p, name="test", value="testkey", enabled=True)

        p = cls.create_person("dd_nu", status=const.STATUS_DD_NU)
        cls.keys.create("dd_nu", user=p, name="testdd", value="testkeydd", enabled=True)

    def test_anonymous(self):
        client = self.make_test_client(None)
        self.assertListForbidden(client)
        self.assertEnableForbidden(client, self.keys.dc.pk)
        self.assertDeleteForbidden(client, self.keys.dc.pk)

    def test_nondd(self):
        client = self.make_test_client("dc")
        self.assertListForbidden(client)
        self.assertEnableForbidden(client, self.keys.dc.pk)
        self.assertDeleteForbidden(client, self.keys.dc.pk)

    def test_dd(self):
        client = self.make_test_client("dd_nu")
        self.assertListAllowed(client)
        self.assertEnableAllowed(client, self.keys.dd_nu.pk)
        self.assertEnableAllowed(client, self.keys.dd_nu.pk, 0)
        self.assertEnableAllowed(client, self.keys.dd_nu.pk, 1)
        self.assertDeleteAllowed(client, self.keys.dd_nu.pk)
        self.assertEnableForbidden(client, self.keys.dc.pk, 0)
        self.assertEnableForbidden(client, self.keys.dc.pk, 1)
        self.assertDeleteForbidden(client, self.keys.dc.pk)

    def assertListAllowed(self, client):
        response = client.get(reverse("apikeys:list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(list(response.context["keys"]), list(client.visitor.apikeys.all()))
        self.assertEqual(list(response.context["audit_log"]), [])
        self.assertEqual(response.context["audit_log_cutoff_days"], 30)
        self.assertIn("key_create_form", response.context)

    def assertListForbidden(self, client):
        response = client.get(reverse("apikeys:list"))
        self.assertPermissionDenied(response)

    def assertEnableAllowed(self, client, pk, enabled=1):
        response = client.post(reverse("apikeys:enable", args=[pk]), data={"enabled": enabled})
        self.assertRedirectMatches(response, reverse("apikeys:list"))
        key = Key.objects.get(pk=pk)
        self.assertEqual(key.enabled, bool(enabled))

    def assertDeleteAllowed(self, client, pk, enabled=1):
        response = client.post(reverse("apikeys:delete", args=[pk]))
        self.assertRedirectMatches(response, reverse("apikeys:list"))
        with self.assertRaises(Key.DoesNotExist):
            Key.objects.get(pk=pk)

    def assertEnableForbidden(self, client, pk, enabled=1):
        response = client.post(reverse("apikeys:enable", args=[pk]), data={"enabled": enabled})
        self.assertPermissionDenied(response)

    def assertDeleteForbidden(self, client, pk):
        response = client.post(reverse("apikeys:delete", args=[pk]))
        self.assertPermissionDenied(response)
        Key.objects.get(pk=pk)
