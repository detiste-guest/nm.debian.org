from __future__ import annotations
from django.urls import path
from backend.mixins import VisitorTemplateView
from . import views

app_name = "api"

urlpatterns = [
    path('', VisitorTemplateView.as_view(template_name="api/doc.html"), name="doc"),
    path('people/', views.People.as_view(), name="people"),
    path('status/', views.Status.as_view(), name="status"),
    path('whoami/', views.Whoami.as_view(), name="whoami"),
    path('export_identities', views.ExportIdentities.as_view(), name="export_identities"),
    path('salsa_status/<int:subject>', views.SalsaStatus.as_view(), name="salsa_status"),
]
