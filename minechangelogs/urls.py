from django.urls import path
from . import views

app_name = "minechangelogs"

urlpatterns = [
    # Show changelogs (minechangelogs)
    path('search/', views.MineChangelogs.as_view(), name="search_self"),
    path('search/<key>/', views.MineChangelogs.as_view(), name="search"),
]
