import tempfile
import os
from django.test import TestCase, override_settings
from backend import const
from process import models as pmodels
from backend.unittest import PersonFixtureMixin


class TestRequirements(PersonFixtureMixin, TestCase):
    def assertRequirements(self, status, applying_for, expected):
        computed = pmodels.Process.objects.compute_requirements(status, applying_for)
        self.assertCountEqual(computed, expected)

    def assertInvalid(self, status, applying_for):
        with self.assertRaises(RuntimeError):
            pmodels.Process.objects.compute_requirements(status, applying_for)

    def test_requirements(self):
        all_statuses = {s.tag for s in const.ALL_STATUS}

        for dest in all_statuses - {const.STATUS_DC_GA, const.STATUS_DM, const.STATUS_DD_NU, const.STATUS_DD_U}:
            self.assertInvalid("dc", dest)
        self.assertRequirements("dc", const.STATUS_DC_GA, ["intent", "sc_dmup", "advocate", "approval"])
        self.assertRequirements("dc", const.STATUS_DM, ["intent", "sc_dmup", "advocate", "keycheck", "approval"])
        self.assertRequirements("dc", const.STATUS_DD_NU, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])
        self.assertRequirements("dc", const.STATUS_DD_U, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])

        for dest in all_statuses - {const.STATUS_DM_GA, const.STATUS_DD_NU, const.STATUS_DD_U}:
            self.assertInvalid("dc_ga", dest)
        self.assertRequirements("dc_ga", const.STATUS_DM_GA, ["intent", "sc_dmup", "advocate", "keycheck", "approval"])
        self.assertRequirements("dc_ga", const.STATUS_DD_NU, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])
        self.assertRequirements("dc_ga", const.STATUS_DD_U, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])

        for dest in all_statuses - {const.STATUS_DM_GA, const.STATUS_DD_NU, const.STATUS_DD_U}:
            self.assertInvalid("dm", dest)
        self.assertRequirements("dm", const.STATUS_DM_GA, ["intent", "sc_dmup", "approval"])
        self.assertRequirements("dm", const.STATUS_DD_NU, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])
        self.assertRequirements("dm", const.STATUS_DD_U, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])

        for dest in all_statuses - {const.STATUS_DD_NU, const.STATUS_DD_U}:
            self.assertInvalid("dm_ga", dest)
        self.assertRequirements("dm_ga", const.STATUS_DD_NU, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])
        self.assertRequirements("dm_ga", const.STATUS_DD_U, ["intent", "sc_dmup", "advocate", "keycheck", "am_ok", "approval"])

        for dest in all_statuses - set([const.STATUS_DD_U, const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD, "approval"]):
            self.assertInvalid("dd_nu", dest)
        self.assertRequirements("dd_nu", const.STATUS_DD_U, ["intent", "advocate", 'am_ok', "approval"])
        self.assertRequirements("dd_nu", const.STATUS_EMERITUS_DD, ["intent", "approval"])
        self.assertRequirements("dd_nu", const.STATUS_REMOVED_DD, ["intent", "approval"])

        for dest in all_statuses - set([const.STATUS_DD_NU, const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD, "approval"]):
            self.assertInvalid("dd_u", dest)
        self.assertRequirements("dd_u", const.STATUS_DD_NU, ["intent", "approval"])
        self.assertRequirements("dd_u", const.STATUS_EMERITUS_DD, ["intent", "approval"])
        self.assertRequirements("dd_u", const.STATUS_REMOVED_DD, ["intent", "approval"])

        for dest in all_statuses - {const.STATUS_DD_NU, const.STATUS_DD_U}:
            self.assertInvalid("dd_e", dest)
        self.assertRequirements("dd_e", const.STATUS_DD_NU, ["intent", "sc_dmup", "keycheck", "am_ok", "approval"])
        self.assertRequirements("dd_e", const.STATUS_DD_U, ["intent", "sc_dmup", "keycheck", "am_ok", "approval"])

        for dest in all_statuses - {const.STATUS_DD_NU, const.STATUS_DD_U}:
            self.assertInvalid("dd_r", dest)
        self.assertRequirements("dd_r", const.STATUS_DD_NU, ["intent", "sc_dmup", "keycheck", "am_ok", "approval"])
        self.assertRequirements("dd_r", const.STATUS_DD_U, ["intent", "sc_dmup", "keycheck", "am_ok", "approval"])

    def test_mailbox_file(self):
        process = pmodels.Process.objects.create(person=self.persons.dc, applying_for=const.STATUS_DD_NU)

        with tempfile.TemporaryDirectory() as td:
            with override_settings(PROCESS_MAILBOX_DIR=td):
                self.assertIsNone(process.mailbox_file)
                basename = "process-{}.mbox".format(process.pk)

                fname = os.path.join(td, basename)
                with open(fname, "wt"):
                    pass
                self.assertEqual(process.mailbox_file, fname)
                os.unlink(fname)

                fname = os.path.join(td, basename + ".gz")
                with open(fname, "wt"):
                    pass
                self.assertEqual(process.mailbox_file, fname)
                os.unlink(fname)

                fname = os.path.join(td, basename + ".xz")
                with open(fname, "wt"):
                    pass
                self.assertEqual(process.mailbox_file, fname)
                os.unlink(fname)

                fname = os.path.join(td, basename + ".foo")
                with open(fname, "wt"):
                    pass
                self.assertIsNone(process.mailbox_file)
                os.unlink(fname)

                fname = os.path.join(td, basename + ".gz")
                with open(fname, "wt"):
                    pass
                fname = os.path.join(td, basename + ".xz")
                with open(fname, "wt"):
                    pass
                fname = os.path.join(td, basename + ".foo")
                with open(fname, "wt"):
                    pass
                fname = os.path.join(td, basename)
                with open(fname, "wt"):
                    pass
                self.assertEqual(process.mailbox_file, fname)
                os.unlink(fname)

    def test_mailbox_open(self):
        import mailbox
        import email.message
        import gzip
        import lzma
        from backend.email import get_mbox

        with tempfile.TemporaryDirectory() as td:
            # Create a mailbox
            fname = os.path.join(td, "test.mbox")
            mbox = mailbox.mbox(fname, create=True)
            msg = email.message.Message()
            msg["from"] = "test@example.org"
            msg["to"] = "test@example.org"
            msg["subject"] = "test message"
            msg.set_payload("test")
            mbox.add(msg)
            mbox.close()

            with open(fname, "rb") as fd:
                uncompressed = fd.read()
            with gzip.open(fname + ".gz", "wb") as fd:
                fd.write(uncompressed)
            with lzma.open(fname + ".xz", "wb") as fd:
                fd.write(uncompressed)

            with get_mbox(fname) as msgs:
                self.assertEqual(len(msgs), 1)
            with get_mbox(fname + ".gz") as msgs:
                self.assertEqual(len(msgs), 1)
            with get_mbox(fname + ".xz"):
                self.assertEqual(len(msgs), 1)
