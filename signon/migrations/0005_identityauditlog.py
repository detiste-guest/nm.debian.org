# Generated by Django 2.2.11 on 2020-04-15 13:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('signon', '0004_auto_20200414_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='IdentityAuditLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logdate', models.DateTimeField(auto_now_add=True)),
                ('notes', models.TextField(default='')),
                ('changes', models.TextField(default='{}')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('identity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='audit_log', to='signon.Identity')),
            ],
        ),
    ]
