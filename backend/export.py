from . import models as bmodels
from . import serializers as bserializers
import legacy.models as lmodels
import legacy.serializers as lserializers
import process.models as pmodels
import process.serializers as pserializers
import keyring.models as kmodels
import keyring.serializers as kserializers
import sitechecks.models as smodels
import sitechecks.serializers as sserializers
import dsa.models as dmodels
import dsa.serializers as dserializers
from signon import models as snmodels
from signon import serializers as snserializers


def export_db(full=False):
    """
    Export the whole database into a json-serializable structure.

    If full is False, then the output is stripped of privacy-sensitive
    information.
    """
    # Full serializers export all fields, and support simple fields being added
    # to models at a later time

    # Redacted serializers specify an explicit field list, and need expansion
    # if fields are added. This however prevents newly added sensitive fields
    # from being exported by mistake

    people = []
    if full:
        from .serializers import PersonExportSerializer

        PersonSerializer = PersonExportSerializer
    else:
        from .serializers import PersonRedactedExportSerializer

        PersonSerializer = PersonRedactedExportSerializer
    for person in bmodels.Person.objects.all().order_by("ldap_fields__uid", "email"):
        people.append(PersonSerializer(person).data)

    apikeys = []
    if full:
        from apikeys import models as amodels
        from apikeys import serializers as aserializers

        ApiKeySerializer = aserializers.KeyExportSerializer
        for key in amodels.Key.objects.all().order_by("user"):
            apikeys.append(ApiKeySerializer(key).data)

    gpgkeys = []
    if full:
        from keyring import models as kmodels
        from keyring import serializers as kserializers

        GpgKeySerializer = kserializers.KeyExportSerializer
        for key in kmodels.Key.objects.all().order_by("fpr"):
            gpgkeys.append(GpgKeySerializer(key).data)

    inconsistencies = []
    if full:
        from sitechecks import models as smodels
        from sitechecks import serializers as sserializers

        InconsistencySerializer = sserializers.InconsistencyExportSerializer
        for key in smodels.Inconsistency.objects.all().order_by("person", "process"):
            inconsistencies.append(InconsistencySerializer(key).data)

    identities = []

    if full:
        IdentitySerializer = snserializers.IdentityExportSerializer
    else:
        IdentitySerializer = snserializers.IdentityRedactedExportSerializer

    for identity in snmodels.Identity.objects.all().order_by("person"):
        identities.append(IdentitySerializer(identity).data)

    res = {
        "version": 1,
        "people": people,
        "apikeys": apikeys,
        "gpgkeys": gpgkeys,
        "inconsistencies": inconsistencies,
        "identities": identities,
    }

    return res


def load_db(data):
    if data["version"] == 1:
        importer = ImporterV1()
    else:
        raise NotImplementedError(
            "cannot import data exported as version {}".format(data["version"])
        )

    importer.import_all(data)

    return importer


class ImporterV1:
    def __init__(self):
        # People indexed by key
        self.people = {}

        # AM indexed by key
        self.ams = {}

        # AM indexed by key
        self.ldap_fields = {}

        # Fingerprints indexed by fingerprint
        self.fprs = {}

        # API keys
        self.apikeys = []

        # GPG keys
        self.gpgkeys = []

        # Site checks
        self.inconsistencies = []

        # Login Identities
        self.identities = []

        self.context = {"importer": self}

    def import_person(self, data, shallow=False):
        s = bserializers.PersonExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid Person record {}: {}".format(data, s.errors))

        ldap_fields = s.validated_data.pop("ldap_fields")
        am = s.validated_data.pop("am")
        fprs = s.validated_data.pop("fprs")
        audit_log = s.validated_data.pop("audit_log")
        legacy_processes = s.validated_data.pop("legacy_processes")
        # This will be processed later
        s.validated_data.pop("processes")

        lookup_key = data["lookup_key"]
        person = self.people.get(lookup_key)

        if person is None:
            person = bmodels.Person(**s.validated_data)
            person._rels = {
                "am": None,
                "fprs": [],
                "audit_log": [],
                "legacy_processes": [],
                "processes": [],
                "ldap_fields": None,
            }
            self.people[lookup_key] = person

            # Also import OneToOne relations
            if am:
                person._rels["am"] = self.import_am(person, am, lookup_key)
            if ldap_fields:
                person._rels["ldap_fields"] = self.import_ldap_fields(person, ldap_fields, lookup_key)

        if shallow:
            return

        for fpr in fprs:
            person._rels["fprs"].append(self.import_fingerprint(person, fpr))

        for al in audit_log:
            person._rels["audit_log"].append(self.import_person_audit_log(person, al))

        for lp in legacy_processes:
            person._rels["legacy_processes"].append(
                self.import_legacy_process(person, lp)
            )

        return person

    def import_person_processes(self, data):
        s = bserializers.PersonExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid Person record {}: {}".format(data, s.errors))

        processes = s.validated_data.pop("processes")

        lookup_key = data["lookup_key"]
        person = self.people.get(lookup_key)

        if person is None:
            person = bmodels.Person(**s.validated_data)
            person._rels = {
                "am": None,
                "fprs": [],
                "audit_log": [],
                "legacy_processes": [],
                "processes": [],
                "ldap_fields": None,
            }

        for pr in processes:
            person._rels["processes"].append(self.import_process(person, pr))

        return person

    def import_am(self, person, data, person_lookup_key):
        s = bserializers.AMExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid AM record {}: {}".format(data, s.errors))
        am = bmodels.AM(**s.validated_data)
        am._rels = {
            "person": person,
        }
        self.ams[person_lookup_key] = am
        return am

    def import_ldap_fields(self, person, data, person_lookup_key):
        s = dserializers.LDAPFieldsSerializer(data=data)
        if not s.is_valid():
            raise RuntimeError("Invalid LDAPFields record {}: {}".format(data, s.errors))
        s.validated_data.pop("person")
        ldap_fields = dmodels.LDAPFields(**s.validated_data)
        ldap_fields._rels = {
            "person": person,
        }
        self.ldap_fields[person_lookup_key] = ldap_fields
        return ldap_fields

    def import_fingerprint(self, person, data):
        s = bserializers.FingerprintExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid Fingerprint record {}: {}".format(data, s.errors)
            )
        fpr = bmodels.Fingerprint(**s.validated_data)
        fpr._rels = {
            "person": person,
        }
        self.fprs[fpr.fpr] = fpr
        return fpr

    def import_person_audit_log(self, person, data):
        s = bserializers.PersonAuditLogExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid PersonAuditLog record {}: {}".format(data, s.errors)
            )
        author = s.validated_data.pop("author")
        al = bmodels.PersonAuditLog(**s.validated_data)
        al._rels = {
            "person": person,
            "author": self.people[author],
        }
        return al

    def import_legacy_process(self, person, data):
        s = lserializers.ProcessExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid legacy Process record {}: {}".format(data, s.errors)
            )

        log = s.validated_data.pop("log")
        advocates = s.validated_data.pop("advocates")
        manager = s.validated_data.pop("manager")

        p = lmodels.Process(**s.validated_data)
        p._rels = {
            "person": person,
            "manager": self.ams[manager] if manager else None,
            "advocates": [self.people[x] for x in advocates],
            "log": [self.import_legacy_process_log(p, x) for x in log],
        }
        return p

    def import_legacy_process_log(self, process, data):
        s = lserializers.LogExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid legacy Log record {}: {}".format(data, s.errors)
            )

        changed_by = s.validated_data.pop("changed_by")

        p = lmodels.Log(**s.validated_data)
        p._rels = {
            "process": process,
            "changed_by": self.people[changed_by],
        }
        return p

    def import_process(self, person, data):
        s = pserializers.ProcessExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid Process record {}: {}".format(data, s.errors))

        requirements = s.validated_data.pop("requirements")
        ams = s.validated_data.pop("ams")
        log = s.validated_data.pop("log")
        frozen_by = s.validated_data.pop("frozen_by")
        approved_by = s.validated_data.pop("approved_by")
        closed_by = s.validated_data.pop("closed_by")

        p = pmodels.Process(**s.validated_data)
        p._rels = {
            "person": person,
            "frozen_by": self.people[frozen_by] if frozen_by else None,
            "approved_by": self.people[approved_by] if approved_by else None,
            "closed_by": self.people[closed_by] if closed_by else None,
            "requirements": [],
            "ams": [],
            "log": [],
        }

        for req in requirements:
            p._rels["requirements"].append(self.import_requirement(p, req))

        for am in ams:
            p._rels["ams"].append(self.import_amassignment(p, am))

        for le in log:
            p._rels["log"].append(self.import_process_log_entry(p, le))

        return p

    def import_requirement(self, process, data):
        s = pserializers.RequirementExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid Requirement record {}: {}".format(data, s.errors)
            )

        statements = s.validated_data.pop("statements")
        approved_by = s.validated_data.pop("approved_by")

        req = pmodels.Requirement(**s.validated_data)
        req._rels = {
            "approved_by": self.people[approved_by] if approved_by else None,
            "statements": [],
        }

        for stm in statements:
            req._rels["statements"].append(self.import_statement(req, stm))

        return req

    def import_statement(self, requirement, data):
        s = pserializers.StatementExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid Statement record {}: {}".format(data, s.errors))

        uploaded_by = s.validated_data.pop("uploaded_by")
        fpr = s.validated_data.pop("fpr")

        stm = pmodels.Statement(**s.validated_data)
        stm._rels = {
            "uploaded_by": self.people[uploaded_by] if uploaded_by else None,
            "fpr": self.fprs[fpr] if fpr else None,
        }
        return stm

    def import_process_log_entry(self, process, data):
        s = pserializers.LogExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid Log record {}: {}".format(data, s.errors))

        changed_by = s.validated_data.pop("changed_by")
        requirement = s.validated_data.pop("requirement")

        stm = pmodels.Log(**s.validated_data)
        stm._rels = {
            "changed_by": self.people[changed_by] if changed_by else None,
            "requirement": None,
        }

        if requirement is not None:
            for r in process._rels["requirements"]:
                if r.type == requirement:
                    stm._rels["requirement"] = r

        return stm

    def import_amassignment(self, process, data):
        s = pserializers.AMAssignmentExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid AMAssignment record {}: {}".format(data, s.errors)
            )

        am = s.validated_data.pop("am")
        assigned_by = s.validated_data.pop("assigned_by")
        unassigned_by = s.validated_data.pop("unassigned_by")

        ama = pmodels.AMAssignment(**s.validated_data)
        ama._rels = {
            "am": self.ams[am] if am else None,
            "assigned_by": self.people[assigned_by] if assigned_by else None,
            "unassigned_by": self.people[unassigned_by] if unassigned_by else None,
        }

        return ama

    def import_gpgkey(self, data):
        s = kserializers.KeyExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError("Invalid GPG Key record {}: {}".format(data, s.errors))

        fpr = s.validated_data.pop("fpr")

        key = kmodels.Key(**s.validated_data)
        key._rels = {
            "fpr": self.fprs[fpr],
        }

        return key

    def import_inconsistency(self, data):
        s = sserializers.InconsistencyExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid Inconsistency record {}: {}".format(data, s.errors)
            )

        person = self.people[s.validated_data.pop("person")]
        process = s.validated_data.pop("process")

        inc = smodels.Inconsistency(**s.validated_data)
        inc._rels = {"person": person, "process": None}

        if process is not None:
            appfor, started = process
            found = None
            for p in person._rels["processes"]:
                if (
                    p.applying_for == appfor
                    and p.started.replace(microsecond=0) == started
                ):
                    found = p
            if not found:
                raise RuntimeError(
                    "Process for {} started {} not found for {}".format(
                        appfor, started, person
                    )
                )
            inc._rels["process"] = found

        return inc

    def import_identity(self, data):
        s = snserializers.IdentityExportSerializer(data=data, context=self.context)
        if not s.is_valid():
            raise RuntimeError(
                "Invalid Identity record {}: {}".format(data, s.errors)
            )
        p = s.validated_data.pop("person")
        person = self.people[p] if p else None

        identity = snmodels.Identity(**s.validated_data)
        identity._rels = {"person": person}

        return identity

    def import_all(self, data):
        # We do a double pass to create all people by lookup key, so that they
        # can be referenced in dependent objects
        for person in data["people"]:
            self.import_person(person, shallow=True)
        for person in data["people"]:
            self.import_person(person, shallow=False)
        # Importing processes separately, since this requires fingerprints of
        # all people to be imported
        for person in data["people"]:
            self.import_person_processes(person)

        for apikey in data["apikeys"]:
            pass  # TODO

        for gpgkey in data["gpgkeys"]:
            self.gpgkeys.append(self.import_gpgkey(gpgkey))

        for inconsistency in data["inconsistencies"]:
            self.inconsistencies.append(self.import_inconsistency(inconsistency))

        for identity in data["identities"]:
            self.identities.append(self.import_identity(identity))

    def save(self):
        for person in self.people.values():
            person.save(audit_skip=True)

        for am in self.ams.values():
            am.person = am._rels["person"]
            am.save()

        for ldap_fields in self.ldap_fields.values():
            ldap_fields.person = ldap_fields._rels["person"]
            ldap_fields.save(audit_skip=True)

        for fpr in self.fprs.values():
            fpr.person = fpr._rels["person"]
            fpr.save(audit_skip=True)

        for person in self.people.values():
            for process in person._rels["legacy_processes"]:
                process.person = person
                process.manager = process._rels["manager"]
                process.save()
                process.advocates.set(process._rels["advocates"])

                for log in process._rels["log"]:
                    log.process = process
                    log.changed_by = log._rels["changed_by"]
                    log.save()

            for process in person._rels["processes"]:
                process.person = person
                process.frozen_by = process._rels["frozen_by"]
                process.approved_by = process._rels["approved_by"]
                process.closed_by = process._rels["closed_by"]
                process.save()

                for req in process._rels["requirements"]:
                    req.process = process
                    req.approved_by = req._rels["approved_by"]
                    req.save()

                    for st in req._rels["statements"]:
                        st.requirement = req
                        st.uploaded_by = st._rels["uploaded_by"]
                        st.fpr = st._rels["fpr"]
                        st.save()

                for am in process._rels["ams"]:
                    am.process = process
                    am.am = am._rels["am"]
                    am.assigned_by = am._rels["assigned_by"]
                    am.unassigned_by = am._rels["unassigned_by"]
                    am.save()

                for le in process._rels["log"]:
                    le.process = process
                    le.changed_by = le._rels["changed_by"]
                    le.requirement = le._rels["requirement"]
                    le.save()

        for gpgkey in self.gpgkeys:
            gpgkey.fpr = gpgkey._rels["fpr"]
            gpgkey.save()

        for inconsistency in self.inconsistencies:
            inconsistency.person = inconsistency._rels["person"]
            inconsistency.process = inconsistency._rels["process"]
            inconsistency.save()

        for identity in self.identities:
            identity.person = identity._rels["person"]
            identity.save(audit_skip=True)


# def parse_datetime(s):
#    if s is None:
#        return None
#    return datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
#
#
# def lookup_person(s):
#    if s.endswith("@debian.org"):
#        return bmodels.Person.objects.get(uid=s[:-11])
#    elif '@' in s:
#        return bmodels.Person.objects.get(email=s)
#    elif re.match(r"(?:0x)?[A-F0-9]{16}", s):
#        if s.startswith("0x"):
#            s = s[2:]
#        return bmodels.Person.objects.get(fpr__endswith=s)
#    elif re.match(r"[A-F0-9]{40}", s):
#        return bmodels.Person.objects.get(fpr__endswith=s)
#    else:
#        return bmodels.Person.lookup(s)
#
#
# class Importer(object):
#    def __init__(self, author):
#        # Audit author
#        self.author = author
#        # Key->Person mapping
#        self.people = dict()
#        # Key->AM mapping
#        self.ams = dict()
#
#    def import_person(self, key, info):
#        """
#        Import Person and AM objects, caching them in self.people and self.ams
#        """
#        p = bmodels.Person(
#            username=info["username"],
#            cn=info["cn"],
#            mn=info["mn"],
#            sn=info["sn"],
#            email=info["email"],
#            uid=info["uid"],
#            is_staff=info.get("is_staff", False),
#            is_superuser=info.get("is_superuser", False),
#            status=info["status"],
#            status_changed=parse_datetime(info["status_changed"]),
#            created=parse_datetime(info["created"]),
#            fd_comment=info["fd_comment"] or "",
#        )
#        if self.author:
#            p.save(audit_author=self.author, audit_notes="imported from json database export")
#            if info["fpr"]:
#                bmodels.Fingerprint.objects.create(fpr=info["fpr"], person=p,
#                                                   audit_author=self.author,
#                                                   audit_notes="imported from json database export")
#        else:
#            p.save(audit_skip=True)
#            if info["fpr"]:
#                bmodels.Fingerprint.objects.create(fpr=info["fpr"], person=p,
#                                                   audit_skip=True)
#        self.people[key] = p
#
#        aminfo = info["am"]
#        if aminfo:
#            am = bmodels.AM(
#                person=p,
#                slots=aminfo["slots"],
#                is_am=aminfo["is_am"],
#                is_fd=aminfo["is_fd"],
#                is_dam=aminfo["is_dam"],
#                is_am_ctte=aminfo["is_am_ctte"],
#                created=parse_datetime(aminfo["created"]),
#            )
#            am.save()
#            self.ams[key] = am
#
#    def import_process(self, person, info):
#        """
#        Import a process for the given person
#        """
#        # Create process
#        pr = lmodels.Process(
#            person=person,
#            applying_as=info["applying_as"],
#            applying_for=info["applying_for"],
#            progress=info["progress"],
#            archive_key=info["archive_key"],
#            is_active=info["is_active"],
#        )
#        if info["manager"]:
#            pr.manager = self.ams[info["manager"]]
#        pr.save()
#
#        # Add advocates
#        for a in info["advocates"]:
#            pr.advocates.add(self.people[a])
#
#        # Add log
#        for li in info["log"]:
#            le = lmodels.Log(
#                process=pr,
#                progress=li["progress"],
#                logdate=parse_datetime(li["logdate"]),
#                logtext=li["logtext"],
#            )
#
#            if li["changed_by"]:
#                le.changed_by = self.people[li["changed_by"]]
#            le.save()
#
#    @transaction.atomic
#    def import_people(self, people):
#        count_people = 0
#        count_procs = 0
#
#        # Pass 1: import people and AMs
#        for info in people:
#            try:
#                if count_people and count_people % 300 == 0:
#                    log.info("%d people imported", count_people)
#                self.import_person(info["key"], info)
#                count_people += 1
#            except Exception as e:
#                import json
#                log.info("Offending record at pass 1: %s (%s)", json.dumps(info, indent=2), e)
#                raise
#
#        # Pass 2: import processes and logs
#        for info in people:
#            try:
#                person = self.people[info["key"]]
#                for proc in info["processes"]:
#                    if count_procs and count_procs % 300 == 0:
#                        log.info("%d processes imported", count_procs)
#                    self.import_process(person, proc)
#                    count_procs += 1
#            except Exception as e:
#                import json
#                log.info("Offending record at pass 2: %s (%s)", json.dumps(info, indent=2), e)
#                raise
#
#        return dict(
#            people=count_people,
#            procs=count_procs,
#        )
#
#
