from django.urls import path
from django.views.generic import RedirectView
from . import views

app_name = "person"

urlpatterns = [
    path('', RedirectView.as_view(url="/", permanent=True), name="index"),
    path('<key>/', views.Person.as_view(), name="show"),
    path('<key>/edit_ldap/', views.EditLDAP.as_view(), name="edit_ldap"),
    path('<key>/edit_bio/', views.EditBio.as_view(), name="edit_bio"),
    path('<key>/edit_email/', views.EditEmail.as_view(), name="edit_email"),
    # AM preferences editor
    path('<key>/amprofile/', views.AMProfile.as_view(), name="amprofile"),
    path('<key>/certificate/', views.Certificate.as_view(), name="certificate"),
    path('identities/', views.Identities.as_view(), name='identities_self'),
    path('identities/<key>/', views.Identities.as_view(), name='identities'),
]
