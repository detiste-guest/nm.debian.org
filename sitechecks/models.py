from __future__ import annotations
from django.db import models
from backend.models import Person
from process.models import Process
from django.utils import timezone


class InconsistencyManager(models.Manager):
    def found(self, tag, text, person=None, process=None, last_seen=None):
        """
        Signal that an inconsistency has been found
        """
        if last_seen is None:
            last_seen = timezone.now().date()
        if person is None and process is not None:
            person = process.person
        obj, created = self.get_or_create(
                tag=tag, person=person, process=process, text=text, defaults={"last_seen": last_seen})
        if not created:
            obj.last_seen = last_seen
            obj.save()


class Inconsistency(models.Model):
    """
    Tracks an inconsistency found by site checks
    """
    person = models.ForeignKey(Person, null=True, blank=True, related_name="inconsistencies",
                               on_delete=models.CASCADE)
    process = models.ForeignKey(Process, null=True, blank=True, related_name="inconsistencies",
                                on_delete=models.CASCADE)
    first_seen = models.DateField(auto_now_add=True)
    last_seen = models.DateField()
    ignore_until = models.DateField(null=True, blank=True)
    tag = models.TextField()
    text = models.TextField()

    objects = InconsistencyManager()
